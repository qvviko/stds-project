from typing import List

import numpy as np
from collections import namedtuple, deque
import bisect
import reprlib
from tqdm import tqdm

GKValue = namedtuple('GKValue', ['v', 'g', 'delta'])


# GK algorithm. Used for ARASU
class GKStorage:
    # Storage for GK algorithm
    def __init__(self):
        # v-values, gs and ds are the same as described in the paper
        # I handle three separate lists as its easier for our case and saves some time
        self.vs = []
        self.gs = []
        self.ds = []

    def __repr__(self):
        return f'GKStorage(vs={reprlib.repr(self.vs)},gs={reprlib.repr(self.gs)},ds={reprlib.repr(self.ds)})'

    def __getitem__(self, item):
        return GKValue(self.vs[item], self.gs[item], self.ds[item])

    def __setitem__(self, item, value):
        v, g, d = value
        self.vs[item] = v
        self.gs[item] = g
        self.ds[item] = d

    def __len__(self):
        return len(self.vs)

    def insert(self, item, value):
        v, g, d = value
        self.vs.insert(item, v)
        self.gs.insert(item, g)
        self.ds.insert(item, d)

    def pop(self, index):
        v = self.vs.pop(index)
        g = self.gs.pop(index)
        d = self.ds.pop(index)
        return GKValue(v, g, d)


# Class that performs GK algorithm
class GK:

    def __init__(self, eps: float):
        # eps - possible error
        self.eps: float = eps
        self.values: GKStorage = GKStorage()
        self.total_count: int = 0

    def __repr__(self):
        return f"GK(eps={self.eps}, values={reprlib.repr(self.values)}, total_count={self.total_count})"

    def insert(self, v: float):
        self.total_count += 1
        # sorted insert
        index = bisect.bisect_right(self.values.vs, v)

        # If the value is smallest of largest we need to set delta to 0
        if index == 0 or index == len(self.values):
            delta = 0
        else:
            _, g_i, d_i = self.values[index]
            delta = g_i + d_i - 1
        self.values.insert(index, GKValue(v, 1, delta))

        # every 1/2eps we compress (at least try to) the values
        # This is an arbitrary number that doesn't often change the size of the values
        if self.total_count % (1 / (2 * self.eps)) == 0:
            self.compress()

    def compress(self):
        # Compression algorithm
        # The purpose is to preserve the property of g_i + delta_i < 2*eps*N that preserves the accuracy of (phi-eps)N
        # Therefore if there some items nearby that have g_i + g_{i-1} +delta_i < 2*eps*N they can be merged together
        threshold = 2 * self.eps * self.total_count

        # Start from N-1 as N-1 can't be merged, we always preserve N0 and N-1
        i = len(self.values) - 2
        while i > 0:
            v_i, g_i, d_i = self.values[i]
            total_g = g_i

            j = i - 1
            total_g += self.values[j].g
            while j >= 1 and total_g + d_i < threshold:
                self.values.pop(j)
                self.values.gs[i - 1] = total_g
                j -= 1
                i -= 1
                total_g += self.values[j].g
            i -= 1

    def quantile(self, q: float):
        # Quantile computation
        if self.total_count == 0 or q > 1 or q < 0:
            return np.nan

        # Rank we need to find
        rank = np.ceil(q * self.total_count)

        # Current possible error
        eps_n = np.floor(self.eps * self.total_count)

        # We just want to find the first elements that's r_max is higher that the rank we are looking for
        # The proof that this will yield the item within eps is done in the paper
        if rank >= self.total_count - eps_n:
            return self.values[-1].v
        else:
            r_min = 0
            i = 0
            for i, val in enumerate(self.values):
                r_min += val.g
                # that the range this item handles is within error r+e
                if r_min + self.values[i].delta > rank + eps_n:
                    break
            if i == 0:
                return self.values[i].v
            else:
                return self.values[i - 1].v


# Class for arasu sketches
# Stores values from the built blocks
class ArasuSketch:
    def __init__(self, gk: GK):
        # Items will be set when we are creating them, then we will transform it into list
        self.values = set()
        # epsilon of gk in the block is eps/2, we need to return it to the original state
        self.eps = gk.eps * 2
        # Number of items in the block (actually constant, but I'm lazy to send it to init as it's available here)
        self.N = len(gk.values)

        # We need to compute 1/eps quantiles that are enough to compute the quantile in terms of another_eps, that is
        # equal to some long expression defined in the paper
        arange = np.arange(self.eps, 1.0 + 1e-8, self.eps)

        # This was for debug but why not? The total number in arange should always be 1/eps
        assert len(arange) == int(np.ceil(1 / self.eps))
        for quant in arange:
            # Compute quantiles from the GK handles blocks
            quant_value = gk.quantile(quant)
            # Save only unique ones
            self.values |= {quant_value}
        self.values = list(self.values)

    def __repr__(self):
        return f"ArasuSketch(values={reprlib.repr(self.values)}, eps={self.eps}, N={self.N})"


class ArasuLayer:

    def __init__(self, i: int, N: int, eps: float, arasu: 'ArasuFSSW'):
        # i - index of the layer
        # N - size of the block
        # eps - epsilon for the layer
        # arasu - original arasuFSSW class
        self.id: int = i
        self.N: int = N
        self.eps: float = eps
        self.arasu: 'ArasuFSSW' = arasu

        # Blocks AND sketches (and their beginning) will be stored in the circular buffer (deque)
        # The window is W and each block have N
        self.blocks: deque[GK or ArasuSketch] = deque(maxlen=int(arasu.W / N))
        self.blocks_start: deque[int] = deque(maxlen=int(arasu.W / N))

    def __repr__(self):
        return f"ArasuLayer(id={self.id}, N={self.N}, eps={self.eps})"

    def insert(self, value: float):
        # Insert new item into the layer

        # If there is no blocks it will create new one
        try:
            cur_block = self.blocks[-1]
            # If last block is a sketch, create a new one
            if type(cur_block) == ArasuSketch:
                self.blocks.append(GK(self.eps / 2))
                self.blocks_start.append(self.arasu.total_size)
            cur_block = self.blocks[-1]
        except IndexError:
            self.blocks.append(GK(self.eps / 2))
            self.blocks_start.append(self.arasu.total_size)
            cur_block = self.blocks[-1]

        cur_block.insert(value)
        # If the block is complete, make a sketch out of it
        if cur_block.total_count == self.N:
            self.blocks[-1] = ArasuSketch(self.blocks[-1])


# Class that will handle arasu algorithm
class ArasuFSSW:

    def __repr__(self):
        return f'ArasuFSSW(W={self.W}, eps={self.eps})'

    def __init__(self, W, eps):
        # W and 1/eps should be a power of 2
        # Check if that's so
        # if not - new_W is chosen to closest largest power and eps is chosen by inequality in the paper
        pow_W = np.log2(W)
        if abs(int(pow_W) - pow_W) > 1e-6:
            new_W = 2 ** (np.ceil(pow_W))
        else:
            new_W = W

        pow_eps = np.log2(1 / eps)
        if abs(int(pow_eps) - pow_eps) > 1e-6:
            new_eps = W * eps / new_W
            new_eps = 2 ** (np.floor(np.log2(new_eps)))
        else:
            new_eps = eps

        # W is size of the window we track
        # eps is error constant
        # L is amount of layers - 1
        # N0 size of block at layer 0
        self.W: int = int(new_W)
        self.eps: float = new_eps
        self.L: int = self.get_L()
        self.N0 = int((self.eps * self.W) / 4)
        self.current_window_size = 0
        self.total_size = 0  # total stream seen

        # N = 2 ** id * (N0)
        # eps = 2**L/(2**i)*eps/(4(L+1))
        # GK should be run with error e_i/2
        self.layers: List[ArasuLayer] = [
            ArasuLayer(i, 2 ** i * self.N0, 2 ** (self.L - i) * eps / (4 * (self.L + 1)), self)
            for i in range(self.L + 1)]

    # L is calculated as follows
    # Formula from the paper
    def get_L(self):
        return int(2 + np.log2(1 / self.eps))

    # insert the item into the asaru (propagate through layers)
    def insert(self, value):
        if self.current_window_size != self.W:
            self.current_window_size = self.current_window_size + 1
        self.total_size += 1
        for layer in self.layers:
            layer.insert(value)

    def get_Q(self):
        # Get the QUantile summary from the asaru class

        # Blocks to return
        blocks: List[ArasuSketch or GK] = []
        # We need to choose the best blocks. The better one are the ones with largest size and smaller epsilon, e.g. the
        # one with higher index. We iterate from them

        # Starting position to start searching for blocks
        # Note that we amy cut some amount of the overall window as they may be under construction

        # Starting position from the beginning of the window
        start_position = self.total_size - self.W + 1
        cur_position = start_position

        # While the end of the window wasn't reached
        while cur_position < self.total_size:
            cur_block = None

            # Find the smallest layer that starts with our position
            for layer in reversed(self.layers):
                try:
                    ind = layer.blocks_start.index(cur_position)
                    if cur_position + layer.N <= self.total_size + 1:
                        cur_block = layer.blocks[ind]
                        cur_position += layer.N
                        break
                except ValueError:
                    continue

            # If none layer starts with our position, we either in the very beginning or in the end
            if cur_block is None:
                # In the beginning we therefore choose first block from the lowest layer
                if cur_position == start_position:
                    cur_block = self.layers[0].blocks[0]
                    cur_position = self.layers[0].blocks_start[0]
                # In the end we choose the last block from the lowest layer
                elif cur_position + self.layers[-1].N <= self.total_size + 1:
                    cur_block = self.layers[-1].blocks[-1]
                    cur_position = self.W
                else:
                    # No more layers available, exit
                    break
            if cur_block is not None:
                blocks.append(cur_block)
        return blocks

    def quantile(self, q):
        # Compute quantile

        # Get all the block
        blocks = self.get_Q()

        sumN = 0
        summaries = []
        # Merge algorithm, we take every values with their weights (defined as eps_i * N_i where i is layer id)
        for block in blocks:
            summary = list(zip(block.values, [block.N * block.eps] * len(block.values)))  # check that this is correct
            sumN += block.N
            summaries += summary

        # Sort by items (we are doing quantiles, right? :) )
        summaries = sorted(summaries, key=lambda x: x[0])

        cur_w = summaries[0][1]
        thresh = np.ceil(q * sumN)
        # To get the right item it should be the first one that adding the weight will pass the threhsold of q(N1+..+Ni)
        # Paper proves that this provides value within error
        for elem, weight in summaries:
            if cur_w < thresh <= (cur_w + weight):
                return elem
            cur_w += weight


# The test for the GK, we don't want to use the bad algorithm
def test_gk():
    # We basically run a stream of 5000 items from random normal and see if the properties are ok
    gk = GK(0.0625)
    np.random.seed(42)
    values = np.random.normal(size=5000)
    acc_error = deque(maxlen=100)
    total = 0
    pbar = tqdm(enumerate(values))
    for ind, v in pbar:
        gk.insert(v)

        # Basic property of GK
        assert np.sum(gk.values.gs) == gk.total_count

        # quant should belong to (0, 1], this is just an example arrangement
        for quant in np.arange(0.10, 1.01, 0.1):
            # By definition the result in quantiles should be around floor((quant - eps)N) and floor((quant - eps)N)

            # Get the rank
            r = int(np.ceil(quant * (ind + 1)))
            # Sort the sequence
            srted = sorted(values[:ind + 1])
            # Get quantile (rank - 1 for python lists)
            hand_computed_quant = srted[r - 1]

            # Get the eps*N
            max_error = int(np.floor(gk.eps * gk.total_count))

            # Compute absolute error (just for statistics)
            answer = gk.quantile(quant)
            error = abs(hand_computed_quant - answer)
            acc_error.append(error)
            try:
                # The value returned by the GK should be inside this error
                assert answer in srted[r - max_error - 1:r + max_error]
                pbar.set_description(f"MAE : {np.mean(acc_error):.2f}" + \
                                     f" | Max Err: {int(np.floor(gk.eps * gk.total_count))}" + \
                                     f" | Items: {len(gk.values)}" + \
                                     f" | Passed: {total} tests ")
                total += 1
            except AssertionError as e:
                print(f"Test failed to find rank {r} with max error {max_error}," + \
                      f"as {answer} not in srted[r - max_error - 1:r + max_error]")
                raise e
    print("DONE")


# Same test for arasu, although a bit different eror formula
def test_arasu():
    arasu = ArasuFSSW(512, 0.0625)
    np.random.seed(42)
    values = np.random.normal(size=5000)
    acc_error = deque(maxlen=100)
    pbar = tqdm(enumerate(values))
    total = 0
    for ind, v in pbar:
        arasu.insert(v)

        assert arasu.total_size == ind + 1
        # Start testing when window will be filled. To remove this requirement you should begin with unbounded arasu,
        # which is out of scope of the project
        if arasu.current_window_size == arasu.W:
            # quant should belong to (0, 1], this is just an example arrangement
            for quant in np.arange(0.10, 1.01, 0.1):
                # By definition the result in quantiles should be around floor((quant - eps)N) and floor((quant - eps)N)

                # Get the rank
                r = int(np.ceil(quant * arasu.current_window_size))
                # Sort the sequence
                window_start = max(arasu.total_size - arasu.W, 0)
                srted = sorted(values[window_start:window_start + arasu.current_window_size])
                assert len(srted) == arasu.current_window_size

                # Get quantile (rank - 1 for python lists)
                hand_computed_quant = srted[r - 1]

                # Get the the error in arausu is eps*W/N (until the window becomes full its different)
                eps = arasu.eps * arasu.W / arasu.current_window_size
                max_error = int(np.floor(eps * arasu.current_window_size))

                # max_error = int(np.floor(arasu.eps * arasu.current_window_size))
                # Compute absolute error (just for statistics)
                answer = arasu.quantile(quant)
                error = abs(hand_computed_quant - answer)
                acc_error.append(error)
                try:
                    # The value returned by the GK should be inside this error
                    assert answer in srted[max(r - max_error - 1, 0):r + max_error]
                    pbar.set_description(f"MAE: {np.mean(acc_error):.2f}" + \
                                         f" | Max Err: {int(np.floor(arasu.eps * arasu.current_window_size))}" + \
                                         f" | Passed: {total} tests")
                    total += 1
                except AssertionError as e:
                    print(f"Test failed to find rank {r} with max error {max_error}, " + \
                          f"as {answer} not in srted[r - max_error - 1:r + max_error], " + \
                          f"perfect item is {srted[r - 1]}")
                    raise e
    print("DONE")


if __name__ == '__main__':
    # Test for the correctness
    test_gk()
    test_arasu()
