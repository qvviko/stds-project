# STDS Project
This repository is an implementation of Fixed-size Sliding Window algorithm for streaming windowed quantiles computing. (Approximate Counts and Quantiles over Sliding Windows, Arasu et. al.)

Project structure
* arasu.py - Contains implementation of the FSSW alogorithm as well as implementation of the GK with some tests to ensure the properties are correct.
* arasu_graphs.ipynb - Jupyter notebook containing graphs of the insertion, quantiles computing time, as well as memory consumption.
